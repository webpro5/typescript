interface Rectangle {
    width: number;
    height: number;
  
}
interface ColorRectangle extends Rectangle{
    color: string;

}
const rectangle : Rectangle = {
    width: 20,
    height: 10
}
console.log(rectangle)

const colorRectangle : ColorRectangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(colorRectangle)

